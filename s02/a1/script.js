//1. Translate the other students from our boilerplate code into their own respective objects.

//2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

//3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

//4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

//5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

//6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

//7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

//8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

//9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

let studentOne = {
  name: "John",
  email: "john@mail.com",
  grades: [89, 84, 78, 88],
  computeAve() {
    let average =
      this.grades.reduce((sum, num) => sum + num, 0) / this.grades.length;
    return average;
  },
  willPass() {
    return this.computeAve() >= 85 ? true : false;
  },
  willPassWithHonors() {
    if (this.computeAve() >= 90) {
      return true;
    } else if (this.computeAve() >= 85) {
      return false;
    } else {
      return undefined;
    }
  },
};

let studentTwo = {
  name: "Joe",
  email: "joe@mail.com",
  grades: [78, 82, 79, 85],
  computeAve() {
    let average =
      this.grades.reduce((sum, num) => sum + num, 0) / this.grades.length;
    return average;
  },
  willPass() {
    return this.computeAve() >= 85 ? true : false;
  },
  willPassWithHonors() {
    if (this.computeAve() >= 90) {
      return true;
    } else if (this.computeAve() >= 85) {
      return false;
    } else {
      return undefined;
    }
  },
};

let studentThree = {
  name: "Jane",
  email: "jane@mail.com",
  grades: [87, 89, 91, 93],
  computeAve() {
    let average =
      this.grades.reduce((sum, num) => sum + num, 0) / this.grades.length;
    return average;
  },
  willPass() {
    return this.computeAve() >= 85 ? true : false;
  },
  willPassWithHonors() {
    if (this.computeAve() >= 90) {
      return true;
    } else if (this.computeAve() >= 85) {
      return false;
    } else {
      return undefined;
    }
  },
};

let studentFour = {
  name: "Jessie",
  email: "jessie@mail.com",
  grades: [91, 89, 92, 93],
  computeAve() {
    let average =
      this.grades.reduce((sum, num) => sum + num, 0) / this.grades.length;
    return average;
  },
  willPass() {
    return this.computeAve() >= 85 ? true : false;
  },
  willPassWithHonors() {
    if (this.computeAve() >= 90) {
      return true;
    } else if (this.computeAve() >= 85) {
      return false;
    } else {
      return undefined;
    }
  },
};

let classOf1A = {
  students: [studentOne, studentTwo, studentThree, studentFour],
  countHonorStudents() {
    let count = 0;
    this.students.forEach((student) => {
      if (student.willPassWithHonors()) {
        count++;
      }
    });
    return count;
  },
  honorsPercentage() {
    return (this.countHonorStudents() / this.students.length) * 100;
  },
  retrieveHonorStudentInfo() {
    let honorStudentInfo = [];
    this.students.forEach((student) => {
      if (student.willPassWithHonors()) {
        honorStudentInfo.push({
          email: student.email,
          aveGrade: student.computeAve(),
        });
      }
    });
    return honorStudentInfo;
  },
  sortHonorStudentsByGradeDesc() {
    return this.retrieveHonorStudentInfo().sort(
      (a, b) => b.aveGrade - a.aveGrade
    );
  },
};
