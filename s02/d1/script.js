/* 
    OOP
        - Everything can be an object
        - A way of programming applications where reusable blueprints called classes are used to create objects.

        Benefits:
            Minimizes repetition
            Improves scalability
            Improves maintainability
            Improves security

        - Object creation is at the heart of OOP.
*/

/* 
    Object Literals

        - We have seen that JS comes with predefined objects. We can also create objects of our own through the use of object literals.
*/

// Use an object literal: {} to create an object representing a user.
/* 
    - encapsulation
    - whenever we add properties or methods to an object, we are performing ENCAPSULATION
    - the organization of information (as properties) and behavior (as methods) to belong to the object that encapsulates them
    - (the scope of encapsulation is denoted by object literals)
*/

/* 
  Spaghetti Code - unorganized
*/

let studentOne = {
  name: "John",
  email: "john@mail.com",
  grades: [89, 84, 78, 88],

  // methods
  /* 
      - add the functionalities available to a student as object methods
      - the keyword "this" refers to the object encapsulating the method where "this is called"
  */
  /* login: function () {
    console.log(`${this.email} has logged in`);
  }, */
  login() {
    console.log(`${this.email} has logged in`);
  },
  logout() {
    console.log(`${this.email} has logged out`);
  },
  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are ${this.grades}`);
  },

  // Mini-activity
  // Create a function that will get the quarterly average of student one's grades
  average() {
    let average =
      this.grades.reduce((sum, num) => sum + num, 0) / this.grades.length;
    return average;
  },
  // Mini-activity 2
  // Create a function that will return true if average grade is >= 85, false if otherwise
  isPassed() {
    return this.average() >= 85 ? true : false;
  },
  // Mini-activity 3
  // Create a function called willPassWithHonors() that returns true if the student has passed and their average is >=90. The function returns false if either one is not met
  willPassWithHonors() {
    return this.isPassed && this.average() >= 90 ? true : false;
  },
};

// log the content of studentOne's encapsulated information in the console
console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's email is ${studentOne.email}`);
console.log(`student one's quarterly grade averages are ${studentOne.grades}`);
console.log(studentOne);

studentOne.login();
studentOne.logout();
studentOne.listGrades();
console.log(studentOne.average());
console.log(studentOne.isPassed());
console.log(studentOne.willPassWithHonors());
